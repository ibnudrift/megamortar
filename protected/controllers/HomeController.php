<?php

class HomeController extends Controller
{

	public function actions()
	{
		return array(
			'captcha' => array(
				'class' => 'CCaptchaAction',
				'backColor' => 0xFFFFFF,
			),
		);
	}

	public function actionIndex()
	{
		$this->layout = '//layouts/column1';

		$this->render('index', array(
			// 'product'=>$product,
			// 'model'=>$model,
		));
	}
	public function actionAbout()
	{
		$this->layout = '//layouts/column2';
		$this->pageTitle = 'Tentang Kami - '.$this->pageTitle;
		$this->render('about', array());
	}

	public function actionProduk()
	{
		$this->layout = '//layouts/column2';
		$this->pageTitle = 'Produk Kami - '.$this->pageTitle;

		$categorys = ViewCategory::model()->findAll();
		$products = [];

		if (!isset($_GET['category'])) {
			$act_categ = ViewCategory::model()->findAll('`type` = "category" order by id ASC');

			// get products
			$criteria2=new CDbCriteria;
			$criteria2->with = array('description', 'category', 'categories');
			$criteria2->order = 't.urutan ASC';

			foreach ($act_categ as $ken => $valn) {
				$criteria2->addCondition('t.tag LIKE :category_'. $ken, 'OR');
				$criteria2->params[':category_'. $ken] = '%category='.$valn->id.',%';
			}

			$criteria2->addCondition('status = "1"');
			$criteria2->addCondition('description.language_id = :language_id');
			$criteria2->params[':language_id'] = $this->languageID;

			$criteria2->select = "*";
			$criteria2->group = 't.id';
			$products = PrdProduct::model()->findAll($criteria2);

		}else{
			$act_categ = ViewCategory::model()->find('id = :ids', array(':ids'=> $_GET['category']));
			
			// get products
			$criteria2=new CDbCriteria;
			$criteria2->with = array('description', 'category', 'categories');
			$criteria2->order = 't.urutan ASC';
			$criteria2->addCondition('status = "1"');
			$criteria2->addCondition('description.language_id = :language_id');
			$criteria2->params[':language_id'] = $this->languageID;
			
			$criteria2->addCondition('t.tag LIKE :category');
			$criteria2->params[':category'] = '%category='.$act_categ->id.',%';
			$criteria2->select = "*";
			$criteria2->group = 't.id';
			$products = PrdProduct::model()->findAll($criteria2);
		}

		$this->render('produk', array(
					'vm_gudang' => $categorys,
					'act_categ' => $act_categ,
					'products' => $products,
				));
	}

	public function actionProductdetail()
	{
		$this->layout = '//layouts/column2';

		$categorys = ViewCategory::model()->findAll();
		if (isset($_GET['category'])) {
			$act_categ = ViewCategory::model()->find('id = :ids', array(':ids'=> $_GET['category']));
		}

		// get products
		$criteria2=new CDbCriteria;
		$criteria2->with = array('description');
		$criteria2->order = 't.urutan ASC';
		$criteria2->addCondition('status = "1"');
		$criteria2->addCondition('description.language_id = :language_id');
		$criteria2->params[':language_id'] = $this->languageID;
		
		$criteria2->addCondition('t.tag LIKE :category');
		$criteria2->params[':category'] = '%category='.$act_categ->id.',%';
		$criteria2->select = "*";
		$criteria2->group = 't.id';
		$criteria2->addCondition('t.id = :ids');
		$criteria2->params[':ids'] = intval($_GET['id']);
		$product = PrdProduct::model()->find($criteria2);

		// others
		$criteria2=new CDbCriteria;
		$criteria2->with = array('description');
		$criteria2->order = 't.urutan ASC';
		$criteria2->addCondition('status = "1"');
		$criteria2->addCondition('description.language_id = :language_id');
		$criteria2->params[':language_id'] = $this->languageID;
		
		$criteria2->addCondition('t.tag LIKE :category');
		$criteria2->params[':category'] = '%category='.$act_categ->id.',%';
		$criteria2->select = "*";
		$criteria2->group = 't.id';
		$criteria2->addCondition('t.id != :ids');
		$criteria2->params[':ids'] = intval($_GET['id']);
		$criteria2->limit = 4;
		$product_other = PrdProduct::model()->findAll($criteria2);

		$this->pageTitle = $product->description->name .' - Produk - '.$this->pageTitle;

		$this->render('product_detail', array(
					'vm_gudang' => $categorys,
					'act_categ' => $act_categ,
					'data' => $product,
					'product_other' => $product_other,
				));
	}

	public function actionContact()
	{
		$this->layout = '//layouts/column2';
		$this->pageTitle = 'Hubungi Kami - '.$this->pageTitle;

		$this->render('contact', array());
	}

	public function actionError()
	{
		// $this->layout = '//layouts/error';
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else{
				
				$this->layout='//layouts/column2';

				$this->pageTitle = 'Error '.$error['code'].': '. $error['message'] .' - '.$this->pageTitle;

				$this->render('error', array(
					'error'=>$error,
				));
			}
		}

	}

}
