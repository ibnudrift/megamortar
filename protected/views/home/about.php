<section class="outers_fold_cover_top pg_about">
	<div class="out_table">
		<div class="in_table">
			<div class="blocks_text_mid wow fadeInDown">
				<h1>tentang kami</h1>
				<div class="py-2"></div>
				<div class="blc_lines d-block mx-auto"></div>
			</div>
		</div>
	</div>
</section>

<section class="about_sec_1 back-white">
	<div class="prelative container">
		<div class="contents wow fadeInDown">
			<h3><b>GUDANG MORTAR HADIR UNTUK MENYEMPURNAKAN</b></h3>
			<p>Gudang Mortar dicetuskan dan didirikan atas dasar penyempurnaan produk mortar yang telah beredar di pasar saat ini yang memiliki aspek kekurangan yang sebenarnya dapat ditingkatkan untuk hasil yang lebih sempurna. Para pendiri Gudang Mortar yang memiliki pengalaman gabungan lebih dari 1 abad, adalah praktisi dan pengguna produk mortar yang kemudian menemukan komposisi terbaik dan merupakan penyempurnaan untuk aplikasi di lapangan kerja proyek sesungguhnya. Oleh karena itu, Gudang Mortar merupakan produk yang telah terbukti unggul sertas lebih efisien dalam penggunaannya.</p>
		</div>
		<div class="clear"></div>
	</div>
</section>

<section class="about_sec_2">
	<div class="prelative container">
		<div class="contents blocks wow fadeInDown">
			<div class="row">
				<div class="col-md-33">
					<div class="d-block d-sm-none">
						<img src="<?php echo $this->assetBaseurl ?>banner-about-1.jpg" alt="" class="img img-fluid">
						<div class="d-block d-sm-none py-3 my-1"></div>
					</div>
				</div>
				<div class="col-md-27">
					<div class="py-4 my-3"></div>
					<h4><b>TENTANG GUDANG MORTAR</b></h4>
					<div class="py-2 my-2"></div>
					<div class="blc_lines"></div>
					<div class="py-2 my-2"></div>
					<p>Berangkat dari kombinasi berbagai latar belakang, para pendiri Gudang Mortar memiliki 1 hal yang sama, yakni penguasaan pekerjaan konstruksi dan pembangunan di bidang infrastruktur. Gudang Mortar merupakan jawaban dan pembuktian dari hasil praktek lapangan yang telah dikaji dan dipraktekkan secara nyata dalam berbagai mega proyek.</p>
					<p>Gudang Mortar juga telah mendapatkan kepercayaan dari berbagai perusahaan kontraktor di Indonesia untuk menyediakan solusi produk yang kuat, tahan cuaca, dan awet. Dibantu oleh tenaga profesional dan teknologi manufaktur yang terkini, Gudang Mortar mampu bersaing dan membuktikan semua yang telah dinyatakan. </p>
					<p>Tidak kenal kata berhenti, Gudang Mortar akan selalu menawarkan inovasi produk baru secara terus menerus, ini adalah komitmen kami dalam mengikuti perkembangan zaman dan selalu mencari terobosan baru untuk meningkatkan efisiensi.</p>

				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</section>

<div class="py-2 my-1"></div>
<section class="about_sec_3">
	<div class="prelative container">
		<div class="contents blocks wow fadeInDown">
			<div class="row py-5">
				<div class="col-md-38">
					<div class="py-5"></div>
					<div class="py-5"></div>
					<div class="picts"><img src="<?php echo $this->assetBaseurl ?>lgo-gm-forn_middle.png" alt="" class="img img-fluid"></div>
					<div class="py-2"></div>
					<h5>KAMI ADALAH GUDANG MORTAR.</h5>
					<h4>KEBERHASILAN ANDA, <b>ADALAH GOAL KAMI.</b></h4>
					<div class="clear"></div>
				</div>
				<div class="col-md-22"></div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</section>

<section class="about_sec_4">
	<div class="prelative container">
		<div class="contents blocks py-5 my-5 wow fadeInDown">
			<div class="row py-5 my-5">
				<div class="col-md-60 text-center py-5">
					<div class="logons_middle_tengah mx-auto d-block">
						<img src="<?php echo $this->assetBaseurl.'lgo-gm-forn_middle_whites.png'; ?>" alt="" class="img img-fluid d-block mx-auto">
					</div>
					<div class="py-2"></div>
					<h4><b>KAMI EKSEKUSI. KAMI IMPROVISASI. KAMI INOVASI.</b></h4>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</section>

<section class="about_sec_5 back-white">
	<div class="prelative container py-4">
		<div class="contents blocks wow fadeInDown">
			
			<h4><b>VISI & MISI MEGA MORTAR</b> <br>
			ARAH KOMPAS KAMI</h4>
			<div class="py-2 my-1"></div>
			<div class="blc_lines"></div>
			<div class="py-3 my-1"></div>

			<div class="row">
				<div class="col-md-30">
					<h5><b>VISI</b></h5>
					<div class="py-2"></div>
					<p>Menjadi pilihan utama serta menjadi rekan yang handal dan berjangka panjang untuk terus mendukung dunia pembangunan infrastruktur dengan menghadirkan aneka solusi produk mortar yang inovatif dan efisien.</p>
				</div>
				<div class="col-md-30">
					<div class="d-block d-sm-none py-4"></div>
					<h5><b>MISI</b></h5>
					<div class="py-2"></div>
					<p>Menyediakan produk produk mortar yang terspesifikasi untuk aneka penggunaan spesifik, agar memudahkan penggunaan serta memberikan kepercayaan akan kualitas, baik dalam praktek pemakaian maupun kualitas layanan purna jual untuk semua pelanggan kami.</p>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</section>