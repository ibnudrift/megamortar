<section class="page-title-blog">
    <div class="prelative container">
        <div class="content">
            <h1>Blog</h1>


        </div>
    </div>
</section>

<section class="blog-sec-1">
    <div class="prelative container">
        <div class="row">
            <div class="col-md-45">
                <h1>Showing from the latest articles</h1>
            </div>
            <div class="col-md-15">
                <div class="pagination">
                    <p>Page</p>
                    <a class="active" href="#">1</a>
                    <a href="#">2</a>
                    <a href="#">3</a>
                </div>
            </div>
            <div class="margin"></div>
            <?php for ($i = 0; $i < 4; $i++) { ?>
                <div class="col-md-30">
                    <div class="box-content my-4">
                        <img class="img-fluid mb-3" src="<?php echo $this->assetBaseurl; ?>blogs_item_03.jpg">
                        <h2 class="mb-3">Our Land Transportation Fleet</h2>
                        <div class="more">
                            <a href="<?php echo CHtml::normalizeUrl(array('/home/blogdetail', 'lang' => Yii::app()->language)); ?>">
                                <p>Read Article</p>
                                <img src="<?php echo $this->assetBaseurl; ?>chevron-dark.png" alt="">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-30">
                    <div class="box-content my-4">
                        <img class="img-fluid mb-3" src="<?php echo $this->assetBaseurl; ?>blogs_item_05.jpg">
                        <h2 class="mb-3">Our Terminal Stevedoring Equipments</h2>
                        <div class="more">
                            <a href="<?php echo CHtml::normalizeUrl(array('/home/blogdetail', 'lang' => Yii::app()->language)); ?>">
                                <p>Read Article</p>
                                <img src="<?php echo $this->assetBaseurl; ?>chevron-dark.png" alt="">
                            </a>
                        </div>
                    </div>
                </div>
            <?php } ?>

        </div>
    </div>
</section>