
<section class="blogdetail-sec-1">
    <div class="prelative container">
        <div class="row">
            <div class="col-md-60">
                <h1 class="my-2">Blog</h1>

            </div>
            <div class="col-md-45 ">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb py-2 m-0 pl-0 pr-0">
                        <li class="breadcrumb-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/home', 'lang' => Yii::app()->language)); ?>">Home</a></li>
                        <li class="breadcrumb-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/blog', 'lang' => Yii::app()->language)); ?>">blog</a></li>
                        <li class="breadcrumb-item" aria-current="page"><a>Our Land Transportation Fleet</a></li>
                    </ol>
                </nav>
            </div>
            <div class="col-md-15 ">
                <a class="back-to-index none" href="<?php echo CHtml::normalizeUrl(array('/home/blog', 'lang' => Yii::app()->language)); ?>">
                    <p>Back to Blog Index</p>
                </a>
            </div>
            <div class="col-md-60">
                <div class="desc-blog my-3">
                    <img class="img-fluid my-4" src="<?php echo $this->assetBaseurl; ?>blogs-bigs_pt.jpg">
                    <h1 class="my-4">Our Land Transportation Fleet</h1>
                    <p class="desc">
                        Harindra’s journey was started from a small establishment and now has turned into one of the biggest logistic company with the biggest fleet in Indonesia. Harindra’s innovation and strategic partnerships with clients create unique relationships that brings the big benefit to our customers. We adapt to the latest world-class technologies to provide economies of scale integrated with efficiency of delivery, bulk storages and high quality services.<br><br>

                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris a lobortis magna, dapibus commodo dui. Sed malesuada orci ante, eget molestie est interdum ac. Sed non tellus eget neque iaculis feugiat. Curabitur mattis eget eros ut vehicula. Morbi dictum et elit sed posuere. Duis ultricies rutrum nisl nec egestas. Sed hendrerit risus posuere massa ultricies lacinia. Ut consequat nulla commodo augue luctus aliquet. Praesent fermentum nec urna in cursus.
                        <br><br>
                        Vivamus arcu purus, pharetra quis lacus cursus, tempor tempor neque. Nam ligula magna, auctor a iaculis sed, dictum sed ante. Mauris tristique viverra lorem ac aliquam. Suspendisse orci quam, pretium et sagittis vel, tristique vel felis. Phasellus sit amet diam at erat tincidunt pharetra. Integer nec sodales metus. Morbi in ligula mauris. Aliquam in lectus sed odio auctor ultrices vehicula vel dolor. Suspendisse nulla lacus, laoreet nec rutrum eu, interdum quis tellus.
                    </p>
                </div>
            </div>

            <div class="col-md-45 col-30">
                <h2>Other Articles</h2>
            </div>
            <div class="col-md-15 col-30">
                <a class="back-to-index" href="<?php echo CHtml::normalizeUrl(array('/home/blog', 'lang' => Yii::app()->language)); ?>">
                    <p>Back to Blog Index</p>
                </a>
            </div>
            <div class="margin"></div>
            <div class="col-md-30">
                <div class="box-content my-4">
                    <img class="img-fluid mb-3" src="<?php echo $this->assetBaseurl; ?>blogs_item_03.jpg">
                    <h2 class="mb-3">Our Land Transportation Fleet</h2>
                    <div class="more">
                        <a href="<?php echo CHtml::normalizeUrl(array('/home/blogdetail', 'lang' => Yii::app()->language)); ?>">
                            <p>Read Article</p>
                            <img src="<?php echo $this->assetBaseurl; ?>chevron-dark.png" alt="">
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-30">
                <div class="box-content my-4">
                    <img class="img-fluid mb-3" src="<?php echo $this->assetBaseurl; ?>blogs_item_05.jpg">
                    <h2 class="mb-3">Our Terminal Stevedoring Equipments</h2>
                    <div class="more">
                        <a href="<?php echo CHtml::normalizeUrl(array('/home/blogdetail', 'lang' => Yii::app()->language)); ?>">
                            <p>Read Article</p>
                            <img src="<?php echo $this->assetBaseurl; ?>chevron-dark.png" alt="">
                        </a>
                    </div>
                </div>
            </div>

        </div>
</section>