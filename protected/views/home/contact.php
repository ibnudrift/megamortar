<section class="outers_fold_cover_top pg_contact">
    <div class="out_table">
        <div class="in_table">
            <div class="blocks_text_mid wow fadeInDown">
                <h1>hubungi kami</h1>
                <div class="py-2"></div>
                <div class="blc_lines d-block mx-auto"></div>
            </div>
        </div>
    </div>
</section>

<section class="contact_sec_1 back-white">
    <div class="prelative container">
        <div class="contents wow fadeInDown">
            <h3><b>KAMI AKAN MEMBANTU ANDA...</b></h3>
            <div class="py-2 my-1 d-none d-sm-block"></div>

            <div class="row">
                <div class="col-md-30">
                    <h4><b>PT. MEGA MORTAR INDONESIA</b></h4>
                    <div class="py-2 my-2"></div>
                    <div class="blc_lines"></div>
                    <div class="py-2 my-2"></div>

                    <p><strong>Kantor Pusat Pemasaran<br></strong>Jl. Arimbi no 75C <br>
                    Ds Mbrajan RT 1 RW 4 <br>
                    Kelurahan Noborejo Kec. Argomulyo <br>SALATIGA, 50736</p>
                    <p><strong>Telepon.</strong><br>+62 241 5668877</p>
                    <p><strong>Email.</strong><br><a href="mailto:info@gudangmortar.com">info@gudangmortar.com</a></p>
                </div>
                <div class="col-md-30">
                    <div class="rights_info">
                        <p>Staf relasi dan sales Gudang Mortar akan selalu siaga dalam menjawab berbagai inkuiri anda yang berkaitan dengan informasi produk, kerjasama keagenan, maupun tender project. Silahkan anda dapat menghubungi tim kami secara cepat melalui nomor whatsapp kami berikut:</p>
                        <div class="blocs_wa">
                            <a target="_blank" href="<?php echo $this->nomer_wa_link ?>"><i class="fa fa-whatsapp"></i> Whatsapp <?php echo $this->nomer_wa ?></a>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>

        </div>
        <div class="clear"></div>
    </div>
</section>