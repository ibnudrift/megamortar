<section class="page-title-facilities">
    <div class="prelative container">
        <div class="content">
            <h1>Facilities</h1>
            <p>Harindra group never stop investing in infrastructure development and operational facilities, this is a testament to our commitment to better serving our customers. All existing facilities will always get the best quality check and maintenance for maximum efficiency of work.</p>
        </div>
    </div>
</section>
<section class="facilities-sec-1">
    <div class="prelative container">
        <h1>“Harindra group’s facilities are designed and properly maintained to
            increase operational efficiency and reduce any risk of activity delay ”</h1>
        <p>Harindra Group</p>
    </div>
</section>

<?php 
    $data_fac = [
                    [
                        'picture' =>'facilitys_item_1.jpg',
                        'title'   =>'Our Land Transportation Fleet',
                        'info'    =>'<ul class="p-0"><li class="my-3"><img class="mx-2" src="'. $this->assetBaseurl.'list-line.png">dump trucks</li><li class="my-3"><img class="mx-2" src="'. $this->assetBaseurl.'list-line.png">cargo trucks</li><li class="my-3"><img class="mx-2" src="'. $this->assetBaseurl.'list-line.png">containers</li></ul>',
                        'folder'=>'land-transportation-fleet',
                        'l_gallery' =>['land-transport-logistic-fleet-harindra-01.jpg', 'land-transport-logistic-fleet-harindra-02.jpg', 'land-transport-logistic-fleet-harindra-03.jpg', 'land-transport-logistic-fleet-harindra-04.jpg'],
                    ],
                    [
                        'picture' =>'facilitys_item_2.jpg',
                        'title'   =>'Our Terminal Stevedoring Equipments',
                        'info'    =>'<ul class="p-0"><li class="my-3"><img class="mx-2" src="'. $this->assetBaseurl.'list-line.png">BULK CONVEYORs</li><li class="my-3"><img class="mx-2" src="'. $this->assetBaseurl.'list-line.png">CRANE TOWERs</li><li class="my-3"><img class="mx-2" src="'. $this->assetBaseurl.'list-line.png">EXCAVATORs</li></ul>',
                        'folder'=>'terminal-stevedoring',
                        'l_gallery' =>['termina-stevedoring-harindra-01.jpg', 'termina-stevedoring-harindra-02.jpg', 'termina-stevedoring-harindra-03.jpg', 'termina-stevedoring-harindra-04.jpg', 'termina-stevedoring-harindra-05.jpg'],
                    ],
                    [
                        'picture' =>'facilitys_item_3.jpg',
                        'title'   =>'Our Storage Warehouses',
                        'info'    =>'<ul class="p-0"><li class="my-3"><img class="mx-2" src="'. $this->assetBaseurl.'list-line.png">over 200 hectares of combined dry warehouses</li><li class="my-3"><img class="mx-2" src="'. $this->assetBaseurl.'list-line.png">filling & bagging machine</li><li class="my-3"><img class="mx-2" src="'. $this->assetBaseurl.'list-line.png">stuffing & bulk conveyor machine</li></ul>',
                        'folder'=>'warehouse',
                        'l_gallery' =>['storage-warehouse-&-facilities-harindra-01.jpg', 'storage-warehouse-&-facilities-harindra-02.jpg', 'storage-warehouse-&-facilities-harindra-03.jpg', 'storage-warehouse-&-facilities-harindra-04.jpg', 'storage-warehouse-&-facilities-harindra-05.jpg', 'storage-warehouse-&-facilities-harindra-06.jpg', 'storage-warehouse-&-facilities-harindra-07.jpg', 'storage-warehouse-&-facilities-harindra-08.jpg', 'storage-warehouse-&-facilities-harindra-09.jpg', 'storage-warehouse-&-facilities-harindra-10.jpg', 'storage-warehouse-&-facilities-harindra-11.jpg'],
                    ],
                    [
                        'picture' =>'facilitys_item_4.jpg',
                        'title'   =>'Heavy Duty Equipment Fleet',
                        'info'    =>'<ul class="p-0"><li class="my-3"><img class="mx-2" src="'. $this->assetBaseurl.'list-line.png">excavators</li><li class="my-3"><img class="mx-2" src="'. $this->assetBaseurl.'list-line.png">wheeloaders</li></ul>',
                        'folder'=>'heavy-duty',
                        'l_gallery' =>['harindra-heavy-duty-rental-01.jpg', 'harindra-heavy-duty-rental-02.jpg', 'harindra-heavy-duty-rental-03.jpg', 'harindra-heavy-duty-rental-04.jpg'],
                    ],
                    
                ];
?>

<section class="facilities-sec-2">
    <div class="prelative container">
        <div class="row">
            <?php foreach ($data_fac as $key => $value): ?>
            <div class="col-md-30 ">
                <div class="box-contents">
                    <img class="w-100" src="<?php echo $this->assetBaseurl. $value['picture']; ?>">
                    <h2><?php echo $value['title'] ?></h2>
                    <?php echo $value['info'] ?>
                </div>
                <div class="more">
                    <a data-fancybox="gallery_n<?php echo $key ?>" href="<?php echo $this->assetBaseurl.'../../images/facilities/'.$value['folder'].'/'. $value['l_gallery'][0] ?>">
                        <p>View Gallery</p>
                        <img src="<?php echo $this->assetBaseurl.'button-more.png'; ?>" alt="">
                    </a>
                    <?php foreach ($value['l_gallery'] as $ke2 => $val2): ?>
                        <?php if ($ke2 != 0): ?>
                        <a data-fancybox="gallery_n<?php echo $key ?>" href="<?php echo $this->assetBaseurl.'../../images/facilities/'.$value['folder'].'/'. $val2 ?>"></a>
                        <?php endif ?>
                    <?php endforeach ?>
                </div>
            </div>
            <?php endforeach ?>
        </div>
    </div>
</section>

<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.min.js"></script>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>