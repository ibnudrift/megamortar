<section class="homes_top_cont_1">
	<div class="inners_block">

		<div class="prelative container">
			<div class="row wow fadeInUp">
				<div class="col-md-32">
					<div class="banners"><img src="<?php echo $this->assetBaseurl; ?>banner-tp-products.png" alt="" class="img img-fluid d-block mx-auto"></div>
				</div>
				<div class="col-md-28">
					<div class="py-4 my-1 d-none d-sm-block"></div>
					<div class="py-2 d-block d-sm-none"></div>
					<div class="rights_con">
						<h5>CUKUP BRAND GUDANG MORTAR</h5>
						<p>Untuk aneka aplikasi bangunan anda</p>
						<div class="py-1"></div>
						<a href="<?php echo CHtml::normalizeUrl(array('/home/produk')); ?>" class="btn btns_submits_pin">LIHAT PRODUK gudang mortar</a>
						<div class="clear"></div>
					</div>			
				</div>
			</div>
			<div class="clear"></div>
		</div>

	</div>
</section>

<section class="home_sec_1">
	<div class="prelative container">
		<div class="contents wow fadeInDown">
			<h3><b>gudang mortar</b>, sebuah produk inovatif <br>yang memahami kebutuhan anda</h3>
			<p>Baik anda arsitek, kontraktor, atau pemilik proyek, anda dapat mengandalkan produk gudang mortar untuk aneka keperluan aplikasi seperti perekat batu bata / bata ringan, acian atau kompon, hingga grouter atau pengisi celah serta material waterproofing. Sebagai produk yang diproduksi dengan teknologi terbaru dan material berkualitas, gudang mortar telah memposisikan produknya di urutan teratas namun hadir dengan efisiensi harga terbaik!</p>

			<div class="py-3"></div>
			<a href="<?php echo CHtml::normalizeUrl(array('/home/about')); ?>" class="btn btns_submits_pin">PELAJARI TENTANG KAMI</a>
		</div>
		<div class="clear"></div>
	</div>
</section>

<?php 
$arr_slides = [
                [
                	'pic'=> 'banner/banners_bn_product_dinding.png',
                	'back'=> 'banner/backs_hm_section_dinding.jpg',
                ],
                [
                	'pic'=> 'banner/banners_bn_product_lantai.png',
                	'back'=> 'banner/backs_hm_section_lantai.jpg',
                ],
                [
                	'pic'=> 'banner/banners_bn_product_plafon.png',
                	'back'=> 'banner/backs_hm_section_plafond.jpg',
                ],
                [
                	'pic'=> 'banner/banners_bn_product_waterproof.png',
                	'back'=> 'banner/backs_hm_section_waterproof.jpg',
                ],
              ];
?>
<section class="home_sec_2 customize_back">
	<div class="prelative container">
		<div class="contents wow fadeInDown blocks">
			<div class="blockns_slide_prd">
				<div class="row">
					<div class="col-md-20 blocks_nav_left_fcs">
						<ul class="list-unstyled">
							<li class="l_item data_0 active" data-id="0"><a onclick="return false;" href="#">dinding</a></li>
							<li class="separate"></li>
							<li class="l_item data_1" data-id="1"><a onclick="return false;" href="#">lantai</a></li>
							<li class="separate"></li>
							<li class="l_item data_2" data-id="2"><a onclick="return false;" href="#">plafon & partisi</a></li>
							<li class="separate"></li>
							<li class="l_item data_3" data-id="3"><a onclick="return false;" href="#">Waterproofing</a></li>
							<li class="separate"></li>
						</ul>
					</div>
					<div class="col-md-40">
						<div class="py-5 d-none d-sm-block"></div>
						<!-- <div class="py-5 d-none d-sm-block"></div> -->
						<div class="py-4 d-block d-sm-none"></div>
						<div class="sliders_car">
							<div class="pics">
								<div id="carouselEx2" class="carousel slide" data-ride="carousel" data-interval="6500">
								  <ol class="carousel-indicators d-none">
								    <li data-target="#carouselEx2" data-slide-to="0" class="active"></li>
								    <li data-target="#carouselEx2" data-slide-to="1"></li>
								  </ol>
								  <div class="carousel-inner">
								  	<?php foreach ($arr_slides as $key => $value): ?>
								    <div class="carousel-item car_<?php echo $key ?> <?php if ($key == 0): ?>active<?php endif ?>" data-id="<?php echo $key ?>">
								      <img src="<?php echo $this->assetBaseurl . $value['pic']; ?>" data-back="<?php echo $this->assetBaseurl . $value['back']; ?>" alt="" class="img-fluid d-block mx-auto">
								    </div>
								  	<?php endforeach ?>
								    
								  </div>

								</div>
								<!-- end slide -->

							</div>
							<div class="clear"></div>
						</div>

					</div>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</section>

<div class="py-3"></div>

<section class="home_sec_3">
	<div class="prelative container">
		<div class="contents wow fadeInDown blocks">
			<div class="row">
				<div class="col-md-33 my-auto">
					<div class="d-block d-sm-none">
						<img src="<?php echo $this->assetBaseurl . 'design1_02-07.jpg'; ?>" alt="" class="img img-fluid">
					</div>
					<div class="d-block d-sm-none py-3"></div>
				</div>
				<div class="col-md-27">
					<h4>KEPERCAYAAN TERHADAP PEKERJAAN  KARENA PILIHAN TEPAT ANDA TERHADAP PRODUK GUDANG MORTAR</h4>
					<div class="py-2 my-1"></div>
					<div class="blc_lines"></div>
					<div class="py-2 my-1"></div>
					<p>Gudang mortar berkomitmen untuk memastikan anda bekerja dengan efisien dan berkualitas. Bagi kami, kepercayaan adalah kunci untuk hubungan yang berjangka panjang. </p>
					<p>gudang mortar akan membantu anda tidak hanya dalam pekerjaan yang cepat dan hemat, namun memberi rasa percaya diri karena produk gudang mortar dapat diandalkan kualitasnya dan keawetannya dalam jangka waktu yang sangat panjang.</p>
					<p>Mari bekerjasama dengan gudang mortar dalam kebutuhan proyek anda. Staf relasi kami akan senantiasa siap untuk membantu anda, hubungi kami di hotline:
					<br><b>Whatsapp <?php echo $this->nomer_wa ?> atau</b> <a target="_blank" href="<?php echo $this->nomer_wa_link ?>">klik di sini</a></p>

				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</section>

<section class="home_sec_4 back-white">
	<div class="prelative container">
		<div class="contents wow fadeInDown blocks">
			<div class="row">
				<div class="col-md-60">

					<h4>KEUNGGULAN PRODUK GUDANG MORTAR</h4>
					<h5>AMAN DAN TANPA KOMPROMI</h5>
					<div class="py-2 my-1"></div>
					<div class="blc_lines"></div>
					<div class="py-2 my-1"></div>
					<p>Penguasaan material, operasional produksi serta teknologi yang digunakan gudang mortar <br>
					membawa kualitas produk gudang mortar ke posisi tertinggi di antara produk mortar lainnya.</p>

					<div class="py-3"></div>
					<?php 
					$lists_arr = [
									[
										'pict'=>'btms_icon_1.jpg',
										'info'=>'Kualitas Perekatan Terbaik',
									],
									[
										'pict'=>'btms_icon_2.jpg',
										'info'=>'Cepat Dan Hemat Dalam Pemakaian',
									],
									[
										'pict'=>'btms_icon_3.jpg',
										'info'=>'Kepraktisan Dan Efisiensi',
									],
									[
										'pict'=>'btms_icon_4.jpg',
										'info'=>'Paling Ramah Lingkungan',
									],
								 ];
					?>
					<div class="lists_bloc_compsn">
						<div class="row">
							<?php foreach ($lists_arr as $key => $value): ?>
							<div class="col-md-15 col-30">
								<div class="items">
									<div class="pict"><img src="<?php echo $this->assetBaseurl. $value['pict']; ?>" alt="" class="img img-fluid"></div>
									<div class="info py-2 my-2">
										<h6><?php echo $value['info'] ?></h6>
									</div>
								</div>
							</div>
							<?php endforeach ?>
						</div>
					</div>
					<div class="clear"></div>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</section>

<script>
    $(document).ready(function() {

        $('.blocks_nav_left_fcs ul li.l_item').on('click', function(){
            var ids = parseInt($(this).attr('data-id'));
            $('#carouselEx2').carousel(ids);

            var backs_new = $('#carouselEx2 div.carousel-inner .carousel-item.car_'+ids+' img').attr('data-back');
            var image = $('section.home_sec_2.customize_back');
		    image.fadeOut(750, function () {
		        image.css("background", "url('" + backs_new + "')");
		        image.fadeIn(750);
		    });

            $('.blocks_nav_left_fcs ul li.l_item').removeClass('active');
            $(this).addClass('active');
        });


        $('#carouselEx2').on('slid.bs.carousel', function () {
            var currentIndex = $('#carouselEx2 div.carousel-inner .active').attr('data-id');
            var backs_new = $('#carouselEx2 div.carousel-inner .active img').attr('data-back');
            console.log(backs_new);
            
            $('.blocks_nav_left_fcs ul li.l_item').removeClass('active');
            $('.blocks_nav_left_fcs ul li.l_item.data_' + currentIndex).addClass('active');

            // $('section.home_sec_2.customize_back').css('background-image', "url("+backs_new+")");
            var image2 = $('section.home_sec_2.customize_back');
		    image2.fadeOut(400, function () {
		        image2.css("background", "url('" + backs_new + "')");
		        image2.fadeIn(400);
		    });
        });

    });
</script>