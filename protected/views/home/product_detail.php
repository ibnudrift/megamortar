<section class="tops_product_pndetil">
	<div class="prelatife container">
		<div class="py-5"></div>
		<div class="py-5"></div>

		<div class="contents text-center wow fadeInDown">
			<h6 class="mb-1">PRODUK GUDANG MORTAR</h6>
			<h3 class="mb-0"><b><?php echo strtoupper($act_categ->name); ?></b></h3>
            <div class="py-2 my-1"></div>
            <div class="blc_lines d-block mx-auto"></div>
            <div class="py-2 my-1"></div>
            <h2 class="m-0"><?php echo $data->description->name ?></h2>
            <div class="py-4 my-3"></div>
			<div class="clear"></div>
		</div>
	</div>
</section>

<?php 
$datas = unserialize($data->data);
?>

<section class="prddet_sec_1 back-white">
	<div class="prelative container">
		<div class="contents wow fadeInDown">
			<div class="row">
				<div class="col-md-35 order-2 order-sm-1">
					<h3 class="mb-0"><b><?php echo $data->description->name ?></b></h3>
					<div class="py-2 my-1"></div>
		            <div class="blc_lines"></div>
		            <div class="py-2 my-2"></div>

					<?php echo $data->description->desc; ?>
					<p>
					<?php echo strip_tags($datas['kemasan']) . ", ". strip_tags($datas['masa_kadaluarsa']). " ". strip_tags($datas['penyimpanan']) ?></p>
					
					<div class="py-1"></div>
					<a href="<?php echo CHtml::normalizeUrl(array('/home/contact')); ?>" class="btn btns_submits_pin">INKUIRI / KONTAK KAMI</a>	
					<div class="py-3"></div>
				</div>
				<div class="col-md-25 order-1 order-sm-2">
					<div class="picts_banner_rgh bigs_det">
						<img src="<?php echo $this->assetBaseurl .'../../images/product/'. $data->image; ?>" alt="" class="img img-fluid d-block mx-auto">
						<div class="py-4 d-block d-sm-none"></div>
					</div>
				</div>
			</div>

			<div class="clear"></div>
		</div>
		<div class="clear"></div>
	</div>
</section>

<section class="prddet_sec_2 back-grey py-5">
	<div class="prelative container my-4">
		<div class="py-4"></div>
		<div class="contents wow fadeInDown">
			<div class="row">
				<div class="col-md-30">
					<div class="bloc_sub mb-4 pb-3">
						<h4>Standar Acuan Produk</h4>
						<div class="clear"></div>
						<?php echo $datas['standar_acuan_produk'] ?>
						<div class="clear"></div>
					</div>

					<?php if ($datas['dasar_permukaan']): ?>
					<div class="bloc_sub mb-4 pb-3">
						<h4>Dasar Permukaan</h4>
						<?php echo $datas['dasar_permukaan'] ?>
					</div>
					<?php endif ?>

					<?php if ($datas['cara_pemakaian']): ?>
					<div class="bloc_sub mb-4 pb-3">
						<h4>Cara Pemakaian</h4>
						<?php echo $datas['cara_pemakaian'] ?>
					</div>
					<?php endif; ?>

				</div>
				<div class="col-md-30">
					<div class="bloc_sub mb-4 pb-3">
						<h4>DATA TEKNIS</h4>
						<div class="clear"></div>
						 <?php echo $datas['data_teknik'] ?>
						<div class="clear"></div>
					</div>

					<?php if ($datas['daya_sebar']): ?>
					<div class="bloc_sub mb-4 pb-3">
						<h4>DAYA SEBAR</h4>
						<?php echo $datas['daya_sebar'] ?>
					</div>
					<?php endif; ?>

					<div class="bloc_sub mb-4 pb-3 d-none">
						<h4>DOWNLOAD BROSUR</h4>
						<div class="clear"></div>
						<div class="lists_down_pdf">
							<div class="row">
								<div class="col-12">
									<img src="<?php echo $this->assetBaseurl ?>icn-pdf.png" alt="" class="img img-fluid">
								</div>
								<div class="col">
									<p>Brosur Gudang Mortar <?php echo $data->description->name ?><br>
									<a target="_blank" href="#">Download</a></p>
								</div>
							</div>
						</div>
						<div class="clear"></div>
					</div>

					<div class="clear"></div>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</section>

<?php if (isset($product_other) && count($product_other) > 0): ?>
<section class="products_sec_1 back-white">
    <div class="prelative container">
        <div class="contents wow fadeInDown">
            <div class="row">
                <div class="col-md-30">
                    <h3 class="mb-0"><span class="bluesn"><?php echo strtoupper($act_categ->name); ?> LAINNYA</span></h3>
                </div>
                <div class="col-md-30">
                    <div class="float-right text-right">
                        <form class="form-inline boxsrn_searchs_sline m-0">
                          <label for="inlineFormInputName2" class="mr-3"><b>APLIKASI LAINNYA</b></label>
                          <select name="" id="" class="form-control mb-2 mr-sm-2 checks_cappli">
                            <?php foreach ($vm_gudang as $key => $value): ?>
                            <option <?php if (isset($_GET['category']) && $_GET['category'] == $value->id ): ?>selected="selected"<?php endif ?> value="<?php echo CHtml::normalizeUrl(array('/home/produk', 'category'=> $value->id, 'slug'=>Slug::Create($value->name) )); ?>"><?php echo $value->name ?></option>
                            <?php endforeach ?>
                          </select>
                        </form>
                        <div class="clear"></div>
                    </div>

                </div>
            </div>
            <script type="text/javascript">
            $(function(){
                
                $('select.checks_cappli').change(function(){
                    var n_url = $(this).val();
                    if (n_url !== ''){
                        window.open(n_url, "_SELF");
                    }
                });

            });  
            </script>

            <div class="py-2"></div>
            <div class="blc_lines"></div>
            <div class="py-2 my-1"></div>

            <div class="py-4"></div>
            <!-- start list mortar -->
            <div class="outers_list_products_nmortar">
                <div class="row">
                    <?php foreach ($product_other as $ke => $value) { ?>
                        <div class="col-md-15 col-30">
                            <div class="items text-center mb-4 pb-2">
                                <div class="picts maw173 d-block mx-auto mb-3"><a href="<?php echo CHtml::normalizeUrl(array('/home/productdetail', 'id'=> $value->id, 'slug'=>Slug::Create($value->description->name), 'category'=> $act_categ->id )); ?>"><img src="<?php echo $this->assetBaseurl .'../../images/product/'. $value->image; ?>" alt="<?php echo $value->description->name ?>" class="img img-fluid"></a></div>
                                <div class="infos py-2">
                                    <h4><?php echo $value->description->name ?></h4>
                                    <div class="py-1"></div>
                                    <a href="<?php echo CHtml::normalizeUrl(array('/home/productdetail', 'id'=> $value->id, 'slug'=>Slug::Create($value->description->name), 'category'=> $act_categ->id )); ?>" class="btn btn-link btns_vw_product">LIHAT PRODUK <i class="fa fa-search"></i></a>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <!-- end list mortar -->

            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </div>
</section>
<style type="text/css">
    .maw173{
        max-width: 173px;
    }
</style>
<?php endif ?>
