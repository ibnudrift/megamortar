<section class="outers_fold_cover_top pg_product">
    <div class="out_table">
        <div class="in_table">
            <div class="blocks_text_mid wow fadeInDown">
                <h1>produk gudang mortar</h1>
                <div class="py-2"></div>
                <div class="blc_lines d-block mx-auto"></div>
            </div>
        </div>
    </div>
</section>

<section class="products_sec_1 back-white">
    <div class="prelative container">
        <div class="contents wow fadeInDown">

            <!-- Start List Content -->
            <?php if (isset($_GET['category']) && $_GET['category'] != ''): ?>                
                <div class="row">
                    <div class="col-md-30">
                        <h3 class="mb-0"><b>PRODUK GUDANG MORTAR</b> <br>
                        <span class="bluesn"><?php echo strtoupper($act_categ->name) ?></span></h3>
                    </div>
                    <div class="col-md-30">
                        <div class="py-3"></div>
                        <div class="float-right text-right">
                            <form class="form-inline boxsrn_searchs_sline m-0">
                              <label for="inlineFormInputName2" class="mr-3"><b>APLIKASI LAINNYA</b></label>
                              <select name="" id="" class="form-control mb-2 mr-sm-2 checks_cappli">
                                <?php foreach ($vm_gudang as $key => $value): ?>
                                <option <?php if (isset($_GET['category']) && $_GET['category'] == $value->id ): ?>selected="selected"<?php endif ?> value="<?php echo CHtml::normalizeUrl(array('/home/produk', 'category'=> $value->id, 'slug'=>Slug::Create($value->name) )); ?>"><?php echo $value->name ?></option>
                                <?php endforeach ?>
                              </select>
                            </form>
                            <div class="clear"></div>
                        </div>

                    </div>
                </div>

                <div class="py-2 my-1"></div>
                <div class="blc_lines"></div>
                <div class="py-2 my-1"></div>

                <div class="py-4"></div>
                <!-- start list mortar -->
                <div class="outers_list_products_nmortar">
                    <div class="row">
                        <?php foreach ($products as $ke => $value) { ?>
                            <div class="col-md-15 col-30">
                                <div class="items text-center mb-4 pb-2">
                                    <div class="picts maw173 d-block mx-auto mb-3"><a href="<?php echo CHtml::normalizeUrl(array('/home/productdetail', 'id'=> $value->id, 'slug'=>Slug::Create($value->description->name), 'category'=> $act_categ->id )); ?>"><img src="<?php echo $this->assetBaseurl .'../../images/product/'. $value->image; ?>" alt="<?php echo $value->description->name ?>" class="img img-fluid"></a></div>
                                    <div class="infos py-2">
                                        <h4><?php echo $value->description->name ?></h4>
                                        <div class="py-1"></div>
                                        <a href="<?php echo CHtml::normalizeUrl(array('/home/productdetail', 'id'=> $value->id, 'slug'=>Slug::Create($value->description->name), 'category'=> $act_categ->id )); ?>" class="btn btn-link btns_vw_product">LIHAT PRODUK <i class="fa fa-search"></i></a>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <!-- end list mortar -->
            <?php else: ?>

                <div class="row">
                    <div class="col-md-30">
                        <h3 class="mb-0"><b>PRODUK GUDANG MORTAR</b> <br>
                        <span class="bluesn">SEMUA PRODUK</span></h3>
                    </div>
                    <div class="col-md-30">
                        <div class="py-3"></div>
                        <div class="float-right text-right">
                            <form class="form-inline boxsrn_searchs_sline m-0">
                              <label for="inlineFormInputName2" class="mr-3"><b>APLIKASI LAINNYA</b></label>
                              <select name="" id="" class="form-control mb-2 mr-sm-2 checks_cappli">
                                <option value="">-- Pilih --</option>
                                <?php foreach ($vm_gudang as $key => $value): ?>
                                <option <?php if (isset($_GET['category']) && $_GET['category'] == $value->id ): ?>selected="selected"<?php endif ?> value="<?php echo CHtml::normalizeUrl(array('/home/produk', 'category'=> $value->id, 'slug'=>Slug::Create($value->name) )); ?>"><?php echo $value->name ?></option>
                                <?php endforeach ?>
                              </select>
                            </form>
                            <div class="clear"></div>
                        </div>

                    </div>
                </div>

                <div class="py-2 my-1"></div>
                <div class="blc_lines"></div>
                <div class="py-2 my-1"></div>

                <div class="py-4"></div>
                <!-- start list mortar -->
                <div class="outers_list_products_nmortar">
                    <?php if (count($products) > 0): ?>                        
                    <div class="row">
                        <?php foreach ($products as $ke => $value): ?>
                            <div class="col-md-15 col-30">
                            <span class="d-none">
                                <?php 
                                $texts = array_filter(explode(",", $value->tag));
                                $ncats = str_replace('category=', '', trim($texts[1]));
                                ?>
                            </span>
                                <div class="items text-center mb-4 pb-2">
                                    <div class="picts maw173 d-block mx-auto mb-3"><a href="<?php echo CHtml::normalizeUrl(array('/home/productdetail', 'id'=> $value->id, 'slug'=>Slug::Create($value->description->name), 'category'=> $ncats )); ?>"><img src="<?php echo $this->assetBaseurl .'../../images/product/'. $value->image; ?>" alt="<?php echo $value->description->name ?>" class="img img-fluid"></a></div>
                                    <div class="infos py-2">
                                        <h4><?php echo $value->description->name ?></h4>
                                        <div class="py-1"></div>
                                        <a href="<?php echo CHtml::normalizeUrl(array('/home/productdetail', 'id'=> $value->id, 'slug'=>Slug::Create($value->description->name), 'category'=> $ncats )); ?>" class="btn btn-link btns_vw_product">LIHAT PRODUK <i class="fa fa-search"></i></a>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <?php endif ?>

                </div>
                <!-- end list mortar -->

            <?php endif ?>
            <!-- End List Content -->

            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </div>
</section>

<script type="text/javascript">
$(function(){
    $('select.checks_cappli').change(function(){
        var n_url = $(this).val();
        if (n_url !== ''){
            window.open(n_url, "_SELF");
        }
    });
});  
</script>

<style type="text/css">
    .maw173{
        max-width: 173px;
    }
</style>