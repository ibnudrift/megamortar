<section class="page-title-profile">
    <div class="prelative container">
        <div class="content">
            <h1>Profile</h1>
            <p>Established in 1993, Harindra group has built a reputation as a dynamic logistic corporation consisting of all the realm of logistic such as transportation service, stevedoring, warehousing and heavy equipment rentals. Harindra is growing rapidly in the evolving world of Indonesia’s local and international transportation and logistics development. For decades, we have progressively expanded our logistic network and infrastructure on a national scale.</p>
        </div>
    </div>
</section>

<section class="about-sec-1">
    <div class="background-photos">
        <div class="row no-gutters">
            <div class="col-md-14">
                <img src="<?php echo $this->assetBaseurl; ?>about1-bannerSmall-1.jpg">
            </div>
            <div class="col-md-14">
                <img src="<?php echo $this->assetBaseurl; ?>about1-bannerSmall-2.jpg">
            </div>
            <div class="col-md-14">
                <img src="<?php echo $this->assetBaseurl; ?>about1-bannerSmall-3.jpg">
            </div>
            <div class="col-md-18">
                <img src="<?php echo $this->assetBaseurl; ?>about1-bannerSmall-4.jpg">
            </div>
        </div>
    </div>
</section>


<section class="about-sec-2">
    <div class="prelative container">
        <div class="row">
            <div class="col-md-60 p-1">
                <div class="title">
                    <img src="<?php echo $this->assetBaseurl; ?>divider.png">
                    <h1>WHO WE ARE</h1>
                </div>
            </div>
            <div class="col-md-30 p-1">
                <div class="left-contents">
                    <h3>From Land Transportation
                        To A Full Force Logistics
                        Solution Provider</h3>
                </div>
            </div>
            <div class="col-md-30 px-5 py-2 none">
                <P>
                    Harindra offers a long standing and trusted brand with significant logistic expertise gained over 25 years that we harness to deliver first class logistic solutions for our clients. Our portfolio of projects, past and present, backed up with decades of relationship with the big names in the Indonesian industry, demonstrate Harindra’s ability to deliver better logistic solutions.
                    <br><br>

                    Harindra runs the business in the provision of cargo transportation and logistics services since 1993, that is we gained all the experience in handling various cargo such as bulk cargo, general cargo as well as containers. We own and operate various kind and sufficient equipment, also warehouse, to support the logistic services.
                    <br><br>

                    Our safety and transparent approach during operations can ensure that your business will run smooth, kept free of unwanted problem that needs urgent remedial action. Your management team is then can sit, relax and free to concentrate on building your business, never needed to worry about the logistics, in the knowledge that deliveries, work target, paperworks and procedures are in place, safe and stayed within a fixed budget.
                </P>
            </div>
        </div>
    </div>

    <div class="prelative container cont-full">
        <div class="row">
            <div class="col-md-5"></div>
            <div class="col-md-55 p-0">
                <div class="images-team">
                    <img class="w-100" src="<?php echo $this->assetBaseurl; ?>about2-banner.jpg">
                </div>
            </div>
        </div>
    </div>
</section>

<section class="about-sec-3">
    <div class="prelative container">
        <div class="row">
            <div class="col-md-60 p-1">
                <div class="title">
                    <img src="<?php echo $this->assetBaseurl; ?>divider.png">
                    <h1>OUR STRENGTH</h1>
                </div>
            </div>
            <div class="col-md-30 p-1">
                <div class="left-contents">
                    <h3>People is Our Strength</h3>
                </div>
            </div>
            <div class="col-md-30 px-5 py-2 none">
                <P>
                    The key to our long term success is the quality and service focus of our experienced people. Harindra is led by management team consist of logistic industry veterans that has the combined experience of 50 years in the business. Harindra is committed to strengthening the team through ongoing investment in leadership, staff development, accountability and empowerment of our teams throughout the organization. With our staff fully dedicated to serving our customers, we look to the future with confidence.
                </P>
            </div>
        </div>
    </div>
</section>

<section class="customer-sec">
    <div class="title">
        <div class="prelative container">
            <img src="<?php echo $this->assetBaseurl; ?>divider.png">
            <h1>SOME OF OUR CUSTOMERS</h1>
        </div>
    </div>
    <div class="list-cust">
        <div class="prelative container">
            <ul>
                <li>
                    <img src="<?php echo $this->assetBaseurl; ?>customer-6.jpg">
                </li>
                <li>
                    <img src="<?php echo $this->assetBaseurl; ?>customer-4.jpg">
                </li>
                <li>
                    <img src="<?php echo $this->assetBaseurl; ?>customer-5.jpg">
                </li>
                <li>
                    <img src="<?php echo $this->assetBaseurl; ?>customer-7.jpg">
                </li>
                <li>
                    <img src="<?php echo $this->assetBaseurl; ?>customer-8.jpg">
                </li>
                <li>
                    <img src="<?php echo $this->assetBaseurl; ?>customer-3.jpg">
                </li>
                <li>
                    <img src="<?php echo $this->assetBaseurl; ?>customer-1.jpg">
                </li>
                <li>
                    <img src="<?php echo $this->assetBaseurl; ?>customer-2.jpg">
                </li>

            </ul>
        </div>
    </div>
</section>

<section class="about-sec-4">
    <div class="prelative container">
        <div class="row">
            <div class="col-md-30 p-1">
                <div class="title">
                    <img src="<?php echo $this->assetBaseurl; ?>divider-white.png">
                    <h1>OUR VISION</h1>
                </div>
                <p>Harindra bring more than
                    just a logistic, instead our
                    goal is to bring efficiency
                    that leads to your success.</p>
            </div>
            <div class="col-md-30 p-1">
                
                <div class="d-block d-sm-none py-3 my-2"></div>

                <div class="title">
                    <img src="<?php echo $this->assetBaseurl; ?>divider-white.png">
                    <h1>OUR MISSION</h1>
                </div>
                <p>To provide high dedication logistic solutions and
                    superior customer service with a focus on value
                    added services to ensure the customers get best
                    benefit and success of operations.
                    <br><br>
                    To provide smart investment and tailored service
                    to ease our customers in the long run.</p>
            </div>

        </div>
    </div>
</section>

<section class="about-sec-5">
    <div class="prelative container">
        <div class="row">
            <div class="col-md-60 p-1">
                <div class="title">
                    <img src="<?php echo $this->assetBaseurl; ?>divider-white.png">
                    <h1>OUR COMMITMENT</h1>
                </div>
            </div>
            <div class="col-md-30 p-1">
                <h3>The spirit that embodied
                    as our culture of business
                    in everyday operation</h3>
            </div>
            <div class="col-md-30 p-1">
                <div class="right-contents">
                    <div class="row">
                        <div class="col-md-10 col-15  "> <img src="<?php echo $this->assetBaseurl; ?>about-icon-small-1.png"></div>
                        <div class="col-md-50 col-40 p-0">
                            <p class="sub-heading">Reflection</p>
                            <p>Is the principles to change and become better everyday by looking at our own mistakes and off-performance, then utilize it to be a lesson to achieve optimum result.</p>
                        </div>
                        <div class="col-md-10 col-15"> <img src="<?php echo $this->assetBaseurl; ?>about-icon-small-2.png"></div>
                        <div class="col-md-50 col-40 p-0">
                            <p class="sub-heading">Accountability</p>
                            <p>Become aware and responsible for all actions taken, and always reliable in all situations.</p>
                        </div>
                        <div class="col-md-10 col-15"> <img src="<?php echo $this->assetBaseurl; ?>about-icon-small-3.png"></div>
                        <div class="col-md-50 col-40 p-0">
                            <p class="sub-heading">Pride</p>
                            <p>Having the proudness of becoming a better personel everyday, bringing perfections in every action.</p>
                        </div>
                        <div class="col-md-10 col-15"> <img src="<?php echo $this->assetBaseurl; ?>about-icon-small-4.png"></div>
                        <div class="col-md-50 col-40 p-0">
                            <p class="sub-heading">Glory</p>
                            <p>Working for the success of our customer is our first priority, then glory to our part will follow. </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>