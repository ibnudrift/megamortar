<section class="page-title-services">
    <div class="prelative container">
        <div class="content">
            <h1>Services</h1>
            <p>Harindra group with it’s full realm logistic services can deliver both full solution logistics and even preliminary solutions, tailored on large and complex or a long run projects, involving a strategic logistics infrastructure investment as a strategic mutual partnership.</p>
        </div>
    </div>
</section>

<section class="service-sec-1">
    <div class="prelative container">
        <h1>“Harindra bring more than just a logistic, instead our goal is to bring
            efficiency that leads to your success.”</h1>
        <p>Harindra Group</p>
    </div>
</section>

<section class="service-sec-2">
    <div class="prelative container cont-full">
        <div class="row no-gutters">
            <div class="col-md-5"></div>
            <div class="col-md-25 order-2 order-sm-1">
                <div class="box-contents color-one">
                    <h3>Terminal Stevedoring</h3>
                    <ul class="p-0 mb-0 ">
                        <li class="my-3"><img class="mx-2" src="<?php echo $this->assetBaseurl; ?>chevron.png">stevedoring</li>
                        <li class="my-3"><img class="mx-2" src="<?php echo $this->assetBaseurl; ?>chevron.png">equipment provider</li>
                        <li class="my-3"><img class="mx-2" src="<?php echo $this->assetBaseurl; ?>chevron.png">tallying</li>
                    </ul>
                </div>
            </div>
            <div class="col-md-30 order-1 order-sm-2">
                <img class="img-fluid hv-100 margin-bawah" src="<?php echo $this->assetBaseurl; ?>serv-banner-1.jpg">
            </div>
        </div>
        <div class="row no-gutters">
            <div class="col-md-5"></div>
            <div class="col-md-25 order-2 order-sm-1">
                <div class="box-contents color-two py-5">
                    <h3>Logistic</h3>
                    <ul class="p-0 mb-0">
                        <li class="my-3"><img class="mx-2" src="<?php echo $this->assetBaseurl; ?>chevron.png">Export/Import</li>
                        <li class="my-3"><img class="mx-2" src="<?php echo $this->assetBaseurl; ?>chevron.png">Custom Clearance</li>
                        <li class="my-3"><img class="mx-2" src="<?php echo $this->assetBaseurl; ?>chevron.png">Cargo Distribution</li>
                        <li class="my-3"><img class="mx-2" src="<?php echo $this->assetBaseurl; ?>chevron.png">Dump Truck Service</li>
                        <li class="my-3"><img class="mx-2" src="<?php echo $this->assetBaseurl; ?>chevron.png">Dry Bulk Service
                        </li>
                        <li class="my-3"><img class="mx-2" src="<?php echo $this->assetBaseurl; ?>chevron.png">Container Loading/Unloading
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-30 order-1 order-sm-2">
                <img class="img-fluid hv-100 margin-bawah" src="<?php echo $this->assetBaseurl; ?>serv-banner-2.jpg">
            </div>
        </div>

        <div class="row no-gutters">
            <div class="col-md-5"></div>
            <div class="col-md-25 order-2 order-sm-1">
                <div class="box-contents color-three">
                    <h3>Warehouse</h3>
                    <ul class="p-0 mb-0">
                        <li class="my-3"><img class="mx-2" src="<?php echo $this->assetBaseurl; ?>chevron.png">24 Hours Security System
                        </li>
                        <li class="my-3"><img class="mx-2" src="<?php echo $this->assetBaseurl; ?>chevron.png">Temperature Controlled Storage
                        </li>
                        <li class="my-3"><img class="mx-2" src="<?php echo $this->assetBaseurl; ?>chevron.png">Dry Bulk Specialty
                        </li>
                        <li class="my-3"><img class="mx-2" src="<?php echo $this->assetBaseurl; ?>chevron.png">Packing and Repacking
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-30 order-1 order-sm-2">
                <img class="img-fluid hv-100 margin-bawah" src="<?php echo $this->assetBaseurl; ?>serv-banner-3.jpg">
            </div>
        </div>

        <div class="row no-gutters">
            <div class="col-md-5"></div>
            <div class="col-md-25 order-2 order-sm-1">
                <div class="box-contents color-four">
                    <h3>Heavy Duty Rentals</h3>
                    <ul class="p-0 mb-0">
                        <li class="my-3"><img class="mx-2" src="<?php echo $this->assetBaseurl; ?>chevron.png">Bulldozers</li>
                        <li class="my-3"><img class="mx-2" src="<?php echo $this->assetBaseurl; ?>chevron.png">Excavators</li>
                        <li class="my-3"><img class="mx-2" src="<?php echo $this->assetBaseurl; ?>chevron.png">Wheel Loaders
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-30 order-1 order-sm-2">
                <img class="img-fluid hv-100 margin-bawah" src="<?php echo $this->assetBaseurl; ?>serv-banner-4.jpg">
            </div>
        </div>

    </div>
</section>

<section class="customer-sec">
    <div class="title">
        <div class="prelative container">
            <img src="<?php echo $this->assetBaseurl; ?>divider.png">
            <h1>SOME OF OUR CUSTOMERS</h1>
        </div>
    </div>
    <div class="list-cust">
        <div class="prelative container">
            <ul>
                <li>
                    <img src="<?php echo $this->assetBaseurl; ?>customer-6.jpg">
                </li>
                <li>
                    <img src="<?php echo $this->assetBaseurl; ?>customer-4.jpg">
                </li>
                <li>
                    <img src="<?php echo $this->assetBaseurl; ?>customer-5.jpg">
                </li>
                <li>
                    <img src="<?php echo $this->assetBaseurl; ?>customer-7.jpg">
                </li>
                <li>
                    <img src="<?php echo $this->assetBaseurl; ?>customer-8.jpg">
                </li>
                <li>
                    <img src="<?php echo $this->assetBaseurl; ?>customer-3.jpg">
                </li>
                <li>
                    <img src="<?php echo $this->assetBaseurl; ?>customer-1.jpg">
                </li>
                <li>
                    <img src="<?php echo $this->assetBaseurl; ?>customer-2.jpg">
                </li>

            </ul>
        </div>
    </div>
</section>