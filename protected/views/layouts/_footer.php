<section class="home_sec_5">
    <div class="prelative container">
        <div class="contents blocks wow fadeInDown">
            <h2>apakah anda kontraktor, <br>
            arsitek, atau pemilik proyek? <br>
            Mari menjadi rekan <b>gudang mortar</b>.</h2>
            <h4>kami adalah rekan anda untuk jangka panjang.</h4>
            <div class="clear"></div>
        </div>
    </div>
</section>


<footer class="foot back-white">
    <div class="prelative container wow fadeInUp">
        <div class="py-4 my-2"></div>
        <div class="tops_nfooter">
            <div class="row">
                <div class="col-md-25">
                    <div class="lgo-foot d-inline-block align-bottom mr-3">
                        <img src="<?php echo $this->assetBaseurl; ?>logo-footers.jpg" alt="Logo - gudang mortar">
                    </div>
                    <div class="wans-foot d-inline-block align-bottom">
                        <a target="_blank" href="<?php echo $this->nomer_wa_link ?>"><i class="fa fa-whatsapp"></i> &nbsp; Whatsapp <?php echo $this->nomer_wa ?></a>
                    </div>
                </div>
                <div class="col-md-35">
                    <div class="menus_foot pt-3 mt-2">
                        <ul class="list-inline text-right">
                            <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">home</a></li>
                            <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/about')); ?>">tentang kami</a></li>
                            <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/produk')); ?>">produk gudang mortar</a></li>
                            <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/contact')); ?>">hubungi kami</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="clear clearfix"></div>
        <div class="bottoms_ftsn">
            <div class="row">
                <div class="col-md-30">
                    <p class="m-0">Copyright &copy; <?php echo date("Y"); ?> PT. Mega Mortar Indonesia. All rights reserved.</p>
                </div>
                <div class="col-md-30 text-right">
                    <p class="m-0">Website design by <a title="Website Design Surabaya" target="_blank" href="https://www.markdesign.net/">Mark Design Indonesia</a>.</p>
                </div>
            </div>
        </div>
    </div>
</footer>

<style type="text/css">
    section.live-chat {
        position: fixed;
        right: 0;
        top: 60%;
        z-index: 30;
    }
    .live-chat .live img {
        max-width: 175px;
    }
</style>
<section class="live-chat">
    <div class="row">
        <div class="col-md-60">
            <div class="live">
                <a href="https://wa.me/6281236111922">
                    <img src="<?php echo Yii::app()->baseUrl; ?>/asset/images/icon-whatsapp-float.svg" class="img img-fluid" alt="">
                </a>
            </div>
        </div>
    </div>
</section>