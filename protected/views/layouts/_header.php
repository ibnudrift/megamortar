<?php
$e_activemenu = $this->action->id;
$controllers_ac = $this->id;
$session = new CHttpSession;
$session->open();
$login_member = $session['login_member'];

$active_menu_pg = $controllers_ac . '/' . $e_activemenu;
?>
<header class="head <?php if ($active_menu_pg != 'home/index') : ?>insides-head<?php endif ?>">
    <div class="prelative container cont-header mx-auto">
        <div class="row">
            <div class="col-md-20 col-sm-20">
                <div class="lgo_web_headrs_wb">
                    <a href="<?php echo CHtml::normalizeUrl(array('/home/index', 'lang' => Yii::app()->language)); ?>">
                        <img src="<?php echo $this->assetBaseurl; ?>logo-headers.png" alt="" class="img img-fluid">
                    </a>
                </div>
            </div>
            <div class="col-md-40 text-center">
                <div class="menu-taffix">
                    <ul class="list-inline text-right">
                        <li class="list-inline-item menu"><a href="<?php echo CHtml::normalizeUrl(array('/home/index', 'lang' => Yii::app()->language)); ?>">Home</a></li>
                        <li class="list-inline-item menu"><a href="<?php echo CHtml::normalizeUrl(array('/home/about', 'lang' => Yii::app()->language)); ?>">Tentang Kami</a></li>
                        <li class="list-inline-item menu"><a href="<?php echo CHtml::normalizeUrl(array('/home/produk', 'lang' => Yii::app()->language)); ?>">Produk gudang mortar</a></li>
                        <li class="list-inline-item menu"><a href="<?php echo CHtml::normalizeUrl(array('/home/contact', 'lang' => Yii::app()->language)); ?>">Hubungi Kami</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</header>


<header class="header-mobile homepage_head">
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top">
        <a class="navbar-brand" href="<?php echo CHtml::normalizeUrl(array('/home/index', 'lang' => Yii::app()->language)); ?>"><img src="<?php echo $this->assetBaseurl; ?>logo-headers.png" alt="" class="logo-image"> <b>GUDANG MORTAR</b></a>
        
        <button class="navbar-toggler bg-dark  right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/index', 'lang' => Yii::app()->language)); ?>">Home</a></li>
                <li class="nav-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/about', 'lang' => Yii::app()->language)); ?>">Tentang Kami</a></li>
                <li class="nav-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/produk', 'lang' => Yii::app()->language)); ?>">Produk gudang mortar</a></li>
                <li class="nav-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/contact', 'lang' => Yii::app()->language)); ?>">Hubungi Kami</a></li>
            </ul>
        </div>
    </nav>
</header>