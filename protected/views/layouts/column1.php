<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
<?php echo $this->renderPartial('//layouts/_header', array()); ?>


<?php
// $criteria = new CDbCriteria;
// $criteria->with = array('description');
// $criteria->addCondition('description.language_id = :language_id');
// $criteria->addCondition('active = 1');
// $criteria->params[':language_id'] = $this->languageID;
// $criteria->group = 't.id';
// $criteria->order = 't.id ASC';
// $slide = Slide::model()->with(array('description'))->findAll($criteria);
?>

<div class="fcs-wrapper outers_fcs_wrapper prelatife wrapper-slide">
    <div id="myCarousel_home" class="carousel slide" data-ride="carousel" data-interval="4500">
        <div class="carousel-inner">
            <div class="carousel-item active home-slider-new">
                <img class="img_fcs d-none d-sm-block" src="<?php echo $this->assetBaseurl; ?>slides/fcs-01.jpg" alt="">
                <img class="img_fcs d-block d-sm-none" src="<?php echo $this->assetBaseurl; ?>slides/mobile-fcs-01.jpg" alt="">
            </div>            
        </div>

        <ol class="carousel-indicators d-none">
            <li data-target="#myCarousel_home" data-slide-to="0" class="active"></li>
        </ol>

        <div class="carousel-caption caption-slider-home">
            <div class="prelatife container">
                <div class="bxsl_tx_fcs">
                    <div class="row no-gutters">
                        <div class="col-md-60 text-left">
                            <div class="content wow fadeInUp">
                                <h3>Kami adalah <b>gudang mortar</b>. <br> Kami mengedepankan kualitas dan daya tahan PRODUK di atas segalanya.</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end caption -->
    </div>
</div>
<!-- End fcs -->

<?php echo $content ?>

<?php echo $this->renderPartial('//layouts/_footer', array()); ?>

<script type="text/javascript">
    $(document).ready(function() {

        var heights_full = $(window).height();
        var perc = parseInt((0.22 * heights_full).toFixed(3));

        if ($(window).width() >= 768) {
            $('.homes_top_cont_1').height(perc+"px");
            
            var $item = $('#myCarousel_home.carousel .carousel-item'); 
            var $wHeight = $(window).height() - perc;

            $item.eq(0).addClass('active');
            $item.height($wHeight); 
            $item.addClass('full-screen');

            $('#myCarousel_home.carousel img.d-none.d-sm-block').each(function() {
              var $src = $(this).attr('src');
              var $color = $(this).attr('data-color');
              $(this).parent().css({
                'background-image' : 'url(' + $src + ')',
                'background-color' : $color,
                'background-repeat' : 'no-repeat'
              });
              $(this).remove();
            });

            $(window).on('resize', function (){
              $wHeight = $(window).height() - perc;
              $item.height($wHeight);
            });

            $('#myCarousel_home.carousel').carousel({
              interval: 4000,
              pause: "false"
            });
        }else{
            // slide mobiles
            var $item = $('#myCarousel_home.carousel .carousel-item'); 
            var $wHeight = $(window).height();
            $item.eq(0).addClass('active');
            $item.height($wHeight); 
            $item.addClass('full-screen');

            $('#myCarousel_home.carousel img.d-block.d-sm-none').each(function() {
              var $src = $(this).attr('src');
              $(this).parent().css({
                'background-image' : 'url(' + $src + ')',
                'background-repeat' : 'no-repeat'
              });
              $(this).remove();
            });

            $(window).on('resize', function (){
              $wHeight = $(window).height();
              $item.height($wHeight);
            });

            $('#myCarousel_home.carousel').carousel({
              interval: 4000,
              pause: "false"
            });
        }

    });
</script>

<script>
    $(document).ready(function() {
        $("#click").click(function() {
            $('html, body').animate({
                scrollTop: $("#div1").offset().top
            }, 2000);
        });
    });
</script>

<?php $this->endContent(); ?>