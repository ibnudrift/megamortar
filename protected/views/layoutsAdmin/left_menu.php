<div class="leftmenu">        
    <ul class="nav nav-tabs nav-stacked">
        <li class="nav-header">Navigation</li>
        <li class="dropdown"><a href="<?php echo CHtml::normalizeUrl(array('/admin/product/index')); ?>"><span class="fa fa-tag"></span> <?php echo Tt::t('admin', 'Products') ?></a>
            <ul>
                <!-- <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/static/product')); ?>">All Product Discount</a></li> -->
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/product/index')); ?>">View Products</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/product/create')); ?>">Add Products</a></li>
                
                <?php /*
                    <!-- <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/gallerySpotlight/index')); ?>">Product Spotlight</a></li> -->
                    <!-- <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/filtercat/index')); ?>">Filter</a></li> -->
                    <!-- <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/brand/index')); ?>">Brand</a></li> -->
                    */ ?>
            </ul>
        </li>
        <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/category/index')); ?>"><span class="fa fa-folder"></span> <?php echo Tt::t('admin', 'Category') ?></a></li>
        <?php /*
        <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/promo/index')); ?>"><span class="fa fa-fax"></span> <?php echo Tt::t('admin', 'Voucher Discount') ?></a></li>
        <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/order/index')); ?>"><span class="fa fa-fax"></span> <?php echo Tt::t('admin', 'Orders') ?> (<?php echo OrOrder::model()->count('is_read = 0') ?>)</a></li>
        <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/customer/index')); ?>"><span class="fa fa-fax"></span> <?php echo Tt::t('admin', 'Customer') ?></a></li>
        */ ?>
        <?php /*
            <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/slide/index')); ?>"><span class="fa fa-image"></span> <?php echo Tt::t('admin', 'Slides/Promotion') ?></a></li>
            <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/about/index')); ?>"><span class="fa fa-info"></span> <?php echo Tt::t('admin', 'About Us') ?></a></li>
            <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/about/howto')); ?>"><span class="fa fa-info"></span> <?php echo Tt::t('admin', 'How To Order') ?></a></li>
            <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/gallery/index')); ?>"><span class="fa fa-image"></span> <?php echo Tt::t('admin', 'Event') ?></a></li>
            <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/static/home')); ?>"><span class="fa fa-building"></span> <?php echo Tt::t('admin', 'Home Page') ?></a></li>
            */ ?>

        <?php /*
        <li>&nbsp;</li>
        <li class="dropdown"><a href="<?php echo CHtml::normalizeUrl(array('/admin/slide/index')); ?>"><span class="fa fa-heart"></span> <?php echo Tt::t('admin', 'Slide') ?></a>
            <ul>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/slide/index')); ?>">View Slide</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/slide/create')); ?>">Create Slide</a></li>
            </ul>
        </li>

        <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/address/index')); ?>"><span class="fa fa-building"></span> <?php echo Tt::t('admin', 'Store Locator') ?></a></li>

        <li class="dropdown"><a href="<?php echo CHtml::normalizeUrl(array('/admin/blog/index')); ?>"><span class="fa fa-flag"></span> <?php echo Tt::t('admin', 'Seen On') ?></a>
            <ul>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/blog/index')); ?>">List Seen On</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/blog/create')); ?>">Create Seen On</a></li>
            </ul>
        </li>

        <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/static/merchant')); ?>"><span class="fa fa-folder"></span> <?php echo Tt::t('admin', 'Merchant Partner') ?></a></li>


        <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/static/contact')); ?>"><span class="fa fa-phone"></span> <?php echo Tt::t('admin', 'Contact Us') ?></a></li>

        <!-- class="dropdown" -->
        <li><a href="<?php echo CHtml::normalizeUrl(array('setting/index')); ?>"><span class="fa fa-cogs"></span> <?php echo Tt::t('admin', 'General Setting') ?></a>
        </li>
        */ ?>
        <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/home/logout')); ?>"><span class="fa fa fa-sign-out"></span> Logout</a></li>
    </ul>
</div><!--leftmenu-->
